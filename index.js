// // import CreateElm from './js-files/create'
// //const allElm = require('./js-files/completed')
const dataFromLS = JSON.parse(localStorage.getItem('data'))
let tStatus = JSON.parse(localStorage.getItem('status'))
let list = document.querySelector('ul')
let textBox = document.querySelector('.inputbox')
let data = []
let initialStatus = tStatus;
if (!tStatus) {
    initialStatus = 'active'
}
localStorage.setItem('status', JSON.stringify(initialStatus))
if (dataFromLS) {
    data = dataFromLS;
    displayState()
    activeCount(data)
}
function activeCount(dataBase) {
    let count = 0;
    dataBase.forEach((elem) => {
        if (elem[1] == 'active') {
            count++
        }
    })
    document.querySelector('.bottomtab p').innerText = count + " items left"

}
function displayState() {
    if (initialStatus == 'active') {
        active()
    } else if (initialStatus == 'completed') {
        completed()
    } else {
        all()
    }
}
function create(text, flag) {
    let items = document.createElement('li')
    items.style.position = "relative"
    let status = document.createElement('button')
    status.setAttribute('name', 'status')
    // status.innerText = "/"
    let value = document.createElement('p')
    let del = document.createElement('button')
    del.setAttribute('name', 'delete')
    // del.innerText = "x"
    del.style.backgroundImage = "url('./asserts/xmark-solid.svg')"
    del.style.position = "absolute"
    del.style.right = "0px"
    del.style.backgroundSize = "1em"
    value.innerText = text
    value.style.overflowWrap = "word-break"
    if (flag == true) {
        value.style.textDecoration = "line-through"
        value.style.color = "rgb(151, 151, 151)"
        status.style.backgroundImage = "url('./asserts/circle-check-regular.svg')"
    }

    items.append(status)
    items.append(value)
    items.append(del)
    return items
}
// // if (data.length == 0) {

// //     document.querySelector('.last-box').style.display = "none" //////////not working
// // }
textBox.addEventListener('keyup', (event) => {
    if (event.key == 'Enter' && event.target.value != "") {
        data.push([event.target.value, 'active'])
        localStorage.setItem('data', JSON.stringify(data))
        list.append(create(event.target.value))
        displayState()
        activeCount(data)
        // console.log(data);
        event.target.value = ""
    }
})


let allButton = 1
document.querySelector('.container button').addEventListener('click', (event) => {
    if (allButton % 2 == 1) {
        data = data.map((elem) => {
            return [elem[0], 'completed']
        })
        // console.log(data);
        localStorage.setItem('data', JSON.stringify(data))
        displayState()
        activeCount(data)
    }
    else if (allButton % 2 == 0) {
        data = data.map((elem) => {
            return [elem[0], 'active']
        })
        // console.log(data);
        localStorage.setItem('data', JSON.stringify(data))
        displayState()
        activeCount(data)
    }

    allButton++
})


// // listening button near list of text below
document.querySelector('ul').addEventListener('click', (event) => {
    //console.log(e.target);
    if (event.target.getAttribute("name") == "status") {
        // console.log("status");
        let fu = 0
        data = data.map((elem) => {
            if (elem[0] == event.target.nextElementSibling.innerText && fu == 0) {
                if (elem[1] == 'active') {
                    fu = 1
                    localStorage.setItem('data', JSON.stringify(data))
                    event.target.nextElementSibling.style.textDecoration = "line-through";
                    event.target.style.backgroundImage = "url('./asserts/circle-check-regular.svg')"
                    return [elem[0], 'completed']
                }
                else {
                    localStorage.setItem('data', JSON.stringify(data))
                    event.target.nextElementSibling.style.textDecoration = "none";
                    event.target.style.backgroundImage = "url('./asserts/circle-regular.svg')"
                    return [elem[0], 'active']
                }
            } else {
                return elem;
            }
        })
        displayState()
        activeCount(data)
    }
    if (event.target.getAttribute("name") == "delete") {
        // data[e.target.previousElementSibling.innerText] = "completed"
        // delete data[e.target.previousElementSibling.innerText]
        data = data.filter((elem) => {
            return elem[0] != event.target.previousElementSibling.innerText
        })
        localStorage.setItem('data', JSON.stringify(data))
        event.target.parentElement.remove()
        activeCount(data)
    }
    // console.log(data);
})
// // listening button near list of text babove

// //double click fuctionality below
let temp = ""
document.querySelector('ul').addEventListener("dblclick", (event) => {
    if (event.target.matches('p')) {
        temp = event.target.innerText
        event.target.innerText = ""
        let inputDb = document.createElement('input')
        inputDb.type = "text"
        inputDb.value = temp
        event.target.append(inputDb)
        // console.log(e.target.innerText);
    }
})
document.querySelector('ul').addEventListener('keyup', (event) => {
    if (event.key == 'Enter' && event.target.matches('input')) {
        let fu = true
        data = data.map((elem) => {
            if (fu == true && elem[0] == temp) {
                fu = false
                return [event.target.value, elem[1]]
            } else {
                return elem
            }
        })
        // console.log(e.target.parentElement);
        event.target.parentElement.innerText = event.target.value
        event.target.style.display = "none"
    }
})
// //double click functionality above


// button functionalities below
function all() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });

    data.forEach((elem) => {
        if (elem[1] == 'active') {
            lis.append(create(elem[0]))
        }
        else {
            lis.append(create(elem[0], true))
        }
    })
}
function active() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    data.forEach((elem) => {
        if (elem[1] == 'active') {
            list.append(create(elem[0]))
        }
    })
}
function completed() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    data.forEach((elem) => {
        if (elem[1] == 'completed') {
            list.append(create(elem[0], true))
        }
    })
}
function deleteCompleted() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });

    data = data.filter((elem) => {
        if (elem[1] == 'active') {
            list.append(create(elem[0]))
            return true
        } else {
            return false
        }
    })
    displayState()
    // console.log(data);
}
//button functionalities above


// // console.log(data);

// //button listeners below
document.querySelector('.active-button').addEventListener('click', (event) => {
    initialStatus = 'active'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    active()
})
document.querySelector('.completed-button').addEventListener('click', (event) => {
    initialStatus = 'completed'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    completed()
})
document.querySelector('.all-button').addEventListener('click', (event) => {
    initialStatus = 'all'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    all()
})

document.querySelector('.clear-completed').addEventListener('click', (e) => {
    deleteCompleted()
    console.log(data);
})

// //button listeners above