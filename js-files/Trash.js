// import CreateElm from './js-files/create'
//const allElm = require('./js-files/completed')
const dataFromLS = JSON.parse(localStorage.getItem('data'))
let tStatus = JSON.parse(localStorage.getItem('status'))
let list = document.querySelector('ul')
let textBox = document.querySelector('.inputbox')
let data = {}
let initialStatus = tStatus;
if (!tStatus) {
    initialStatus = 'active'
}
localStorage.setItem('status', JSON.stringify(initialStatus))
if (dataFromLS) {
    data = dataFromLS;
    if (initialStatus == 'active') {
        active()
    } else if (initialStatus == 'completed') {
        completed()
    } else {
        all()
    }
    activeCount(data)
}
function activeCount(dataBase) {
    let count = 0;
    for (let i in dataBase) {
        if (data[i] == 'active') {
            count++
        }
    }
    document.querySelector('.bottomtab p').innerText = count + " items left"

}
function create(text, flag) {
    let items = document.createElement('li')
    items.style.position = "relative"
    let status = document.createElement('button')
    status.setAttribute('name', 'status')
    // status.innerText = "/"
    let value = document.createElement('p')
    let del = document.createElement('button')
    del.setAttribute('name', 'delete')
    // del.innerText = "x"
    del.style.backgroundImage = "url('./asserts/xmark-solid.svg')"
    del.style.position = "absolute"
    del.style.right = "0px"
    del.style.backgroundSize = "1em"
    value.innerText = text
    value.style.overflowWrap = "word-break"
    if (flag == true) {
        value.style.textDecoration = "line-through"
        value.style.color = "rgb(151, 151, 151)"
        status.style.backgroundImage = "url('./asserts/circle-check-regular.svg')"
    }

    items.append(status)
    items.append(value)
    items.append(del)
    return items
}
// if (data.length == 0) {

//     document.querySelector('.last-box').style.display = "none" //////////not working
// }
textBox.addEventListener('keyup', (e) => {
    if (e.key == 'Enter' && e.target.value != "") {
        data[e.target.value] = 'active'
        localStorage.setItem('data', JSON.stringify(data))
        list.append(create(e.target.value))
        if (initialStatus == 'active') {
            active()
        } else if (initialStatus == 'completed') {
            completed()
        } else {
            all()
        }
        activeCount(data)
        e.target.value = ""
    }
})


let allButton = 1
document.querySelector('.container button').addEventListener('click', (e) => {
    if (allButton % 2 == 1) {
        for (let k in data) {
            data[k] = "completed"
            localStorage.setItem('data', JSON.stringify(data))
            if (initialStatus == 'active') {
                active()
            } else if (initialStatus == 'completed') {
                completed()
            } else {
                all()
            }
        }
        activeCount(data)
    }
    else if (allButton % 2 == 0) {
        for (let key in data) {
            data[key] = "active"
            localStorage.setItem('data', JSON.stringify(data))
            if (initialStatus == 'active') {
                active()
            } else if (initialStatus == 'completed') {
                completed()
            } else {
                all()
            }
        }
        activeCount(data)
    }

    allButton++
})
// document.querySelectorAll('li').forEach.addEventListener('click', (e) => {
//     console.log(e.target);
//     if (e.target.name == 'status') {
//         console.log(e.target);


//     }
// })

// console.log(Array.from(document.querySelectorAll('ul li')));

// listening button near list of text below
document.querySelector('ul').addEventListener('click', (e) => {
    //console.log(e.target);
    if (e.target.getAttribute("name") == "status") {
        if (data[e.target.nextElementSibling.innerText] == 'active') {
            data[e.target.nextElementSibling.innerText] = "completed"
            localStorage.setItem('data', JSON.stringify(data))
            e.target.nextElementSibling.style.textDecoration = "line-through";
            e.target.style.backgroundImage = "url('./asserts/circle-check-regular.svg')"
            activeCount(data)
        }
        else {
            data[e.target.nextElementSibling.innerText] = 'active'
            localStorage.setItem('data', JSON.stringify(data))
            e.target.nextElementSibling.style.textDecoration = "none";
            e.target.style.backgroundImage = "url('./asserts/circle-regular.svg')"
            activeCount(data)
        }
    }
    if (e.target.getAttribute("name") == "delete") {
        // data[e.target.previousElementSibling.innerText] = "completed"
        delete data[e.target.previousElementSibling.innerText]
        localStorage.setItem('data', JSON.stringify(data))
        e.target.parentElement.remove()
        activeCount(data)
    }
    // console.log(data);
})
// listening button near list of text babove

//double click fuctionality below
// let temp = ""
// document.querySelector('ul').addEventListener("dblclick", (e) => {
//     if (e.target.matches('p')) {
//         temp = e.target.innerText
//         e.target.innerText = ""
//         let inputDb = document.createElement('input')
//         inputDb.type = "text"
//         inputDb.value = temp
//         e.target.append(inputDb)
//         console.log(e.target.innerText);
//     }
// })
// document.querySelector('ul').addEventListener('keyup', (e) => {
//     if (e.key == 'Enter' && e.target.matches('input')) {

//         // console.log(temp);
//         e.target.parentElement.innerText = e.target.value
//         e.target.style.display = "none"
//         let arr = Object.entries(data).map((elem) => {
//             if (elem[0] == temp) {
//                 return [e.target.value, elem[1]]
//             } else {
//                 return elem
//             }
//         })
//         arr.forEach((elem) => {
//             data[elem[0]] = data[elem[1]]
//         })

//     }
// })
//double click functionality above


// button functionalities below
function all() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    for (let key in data) {
        if (data[key] == 'active') {
            lis.append(create(key))

        }
        else {
            lis.append(create(key, true))
        }
    }
}
function active() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    for (let key in data) {
        if (data[key] == 'active') {
            list.append(create(key))
        }
    }
}
function completed() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    for (let key in data) {
        if (data[key] == 'completed') {
            list.append(create(key, true))
            // console.log(key, true);
        }
    }
}
function deleteCompleted() {
    let lis = document.querySelector('ul')
    Array.from(lis.children).forEach(element => {
        element.remove()
    });
    for (let key in data) {
        if (data[key] == 'active') {
            list.append(create(key))
        }
        else {
            delete data[key]
            localStorage.setItem('data', JSON.stringify(data))
        }
    }
}
//button functionalities above


// console.log(data);

//button listeners below
document.querySelector('.active-button').addEventListener('click', (e) => {
    initialStatus = 'active'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    active()
})
document.querySelector('.completed-button').addEventListener('click', (e) => {
    initialStatus = 'completed'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    completed()
})
document.querySelector('.all-button').addEventListener('click', (e) => {
    initialStatus = 'all'
    localStorage.setItem('status', JSON.stringify(initialStatus))
    all()
})

document.querySelector('.clear-completed').addEventListener('click', (e) => {
    deleteCompleted()
})

//button listeners above