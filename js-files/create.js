function createElm(text) {
    let list = document.querySelector('ul')
    let dropDown = document.createElement('ul')
    dropDown.remove('li')
    let items = document.createElement('li')
    items.style.position = "relative"
    let status = document.createElement('button')
    status.setAttribute('name', 'status')
    // status.innerText = "/"
    let value = document.createElement('p')
    let del = document.createElement('button')
    // del.innerText = "x"
    del.style.backgroundImage = "url('./asserts/xmark-solid.svg')"
    del.style.position = "absolute"
    del.style.right = "0px"
    del.style.backgroundSize = "1em"
    value.innerText = text

    items.append(status)
    items.append(value)
    items.append(del)
    dropDown.append(items)
    list.append(dropDown)
}

module.exports = createElm

